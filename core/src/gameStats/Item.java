package gameStats;

public class Item{
	public String title;
	 int damage;
	int effect;
	int effectProbablity;
	int count;
	public int itemType;
	public int itemID;
	
	public void initAttack(int itemID,int count){
		
		
		switch (itemID){
		
		case Attacks.iATTACK_WHACK:
			 title = "Whack!";
			  damage = 10;
			 effect= Effects.eGROGGY;
			effectProbablity = 20;
			 this.count = count;

			break;
			
		case Attacks.iATTACK_SLASH:
			
			title = "SLASH!";
			 damage = 8;
			 effect= Effects.eBLEEDING;
			 effectProbablity = 30;
			 this.count = count;
			 
			break;
			
		case Attacks.iATTACK_CHAINWHIP:
			title = "WHIP!";
			 damage = 12;
			 effect= Effects.eGROGGY;
			 effectProbablity = 12;
			 this.count = count;
			break;
			
			
		case Attacks.iATTACK_FIRESPIT :
			title = "SPIT FIRE";
			 damage = 20;
			 effect= Effects.eBURN;
			 effectProbablity = 40;
			 this.count = count;
			break;
		
		
		
		}
	
}


}