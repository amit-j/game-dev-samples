package combatEntities;


import entities.Entity;
import gameStats.Attacks;
import handlers.CombatManager;
import handlers.Globals;
import handlers.MiscRepoItem;

public class CombatEntity extends Entity {
	
	
	
	public static final int ceOWNERPLAYER = 0,
							ceOWNERMONSTER =1 ;
	
	
	public static CombatEntity[] CombatEntityList=new CombatEntity[20];
	
	
	
	public int		xSpeed;
	public int		ySpeed;
	public int		myDirection;
	public boolean isSelected;
	public boolean isNewSelection;
	public boolean isUnSelected;
	int selectionMovement = 16;
	boolean selectionMovementUp = false;
	public int type;
	int alphaDelay;  //blinking the charecter after attack :P
	int startX;
	int startY;
	
	public int owner ;
	
	public static final int ceROBOT = 0,
							ceMONSTER  = 1;
	
	int		floatX;
	int		floatY;
	int 	actionDelay;
	
	// stats
	int		health;
	int 	floatXMax;
	
	public int xOffset;
	public int yOffset;

public MiscRepoItem mAttacks;

	
	public final  void init(int type,int owner){
		
		this.type  = type;
		deleted=false;
		died=false;
		isSelected = false;
		isUnSelected = true;
		this.owner = owner;
		floatX = 0;
		floatY  = 0;
		floatXMax = 256;
		actionDelay= 0;
		alpha = 225;
		mAttacks  =new MiscRepoItem();
		
		switch (type)
		{
		
		case ceROBOT:
			
			
			xOffset=20;
			yOffset = 0;
			w = 8;
			h =9;
			mAttacks.add(MiscRepoItem.tATTACK, Attacks.iATTACK_FIRESPIT, 20);
			mAttacks.add(MiscRepoItem.tATTACK, Attacks.iATTACK_CHAINWHIP, 20);
			

			
			break;
			
		case ceMONSTER:

			xOffset=52;
			yOffset = 31;
			w = 8;
			h =9;
			mAttacks.add(MiscRepoItem.tATTACK, Attacks.iATTACK_SLASH, -1);
			mAttacks.add(MiscRepoItem.tATTACK, Attacks.iATTACK_WHACK, -1);

			break;
			
		
		
		}
		
		
	}
	
	public final static void initCombatEntity() {
		for (int i=CombatEntityList.length - 1; i >= 0; i--) CombatEntityList[i]=new CombatEntity();
		killAll();
	}
	
	public final static void killAll() {
		for (int i=CombatEntityList.length - 1; i >= 0; i--) CombatEntityList[i].deleted=true;
	}
	
	public final static int addFighter(int tx,int ty, int type,int owner) {
		int i=0;
		while (i<CombatEntityList.length && !CombatEntityList[i].deleted) i++;
		if (i<CombatEntityList.length) {
			CombatEntityList[i].init(type,owner);
			return i;
		}
		return -1;
	}
	
	
	
	public static void updateCombatEntity(int currentSelection,int mTurn){
		int i=0;
		int selectionCnt = -1;
		int selection = 0;
		
		if (mTurn == CombatManager.INPLAYERSELECT)
			selection = CombatEntity.ceOWNERPLAYER;
		else if(mTurn == CombatManager.INMONSTERSELECT)
		{
			
			selection = CombatEntity.ceOWNERMONSTER;
		}
	
		
		
		while (i<CombatEntityList.length) {
			if (!CombatEntityList[i].deleted  ) {
				if( CombatEntityList[i].owner == selection)
					selectionCnt++;
				if (selectionCnt == currentSelection )
				{
					
					CombatEntityList[i].setSelection();
					selectionCnt  = 99;
				
				}
					else
					CombatEntityList[i].removeSelection();
				CombatEntityList[i].update();
				
				if (CombatEntityList[i].died) {
					// monster died
					//CombatEntityList[i].deleted=true;
				}
				
			}
			i++;
			
		}
	
		
		
	}	
	
	public final void update(){
		
		
		//fancy movement of the selected entity
		if (isSelected  && selectionMovementUp){
			
			floatY-=2;
			//speedY=2;
		}
		else if(isSelected  && !selectionMovementUp){
			
			floatY+=2;
		}
		
		selectionMovement--;
		if 	(selectionMovement < 0)
		{
			
			selectionMovement = 16;
			selectionMovementUp = !selectionMovementUp;
		}
		
		
		switch (owner)
		{
	
		
		
	
		case ceOWNERPLAYER:
			
			if(isSelected  && actionDelay > 0)
			{
				actionDelay--;
				floatX+=16;
				if (floatX > floatXMax)
					floatX = floatXMax;
				
				
			}
		
		
			else if(isUnSelected && actionDelay > 0   ){
				actionDelay--;
				floatX-=16;
				if (floatX< 0) 
				floatX = 0;
			}
			else if (isUnSelected )
			{
				//x=startX ;
			//	y=startY ;
				floatX = 0;
				floatY = 0;
			}
			
			
			
			
			break;
		case ceOWNERMONSTER:
			
			if(isSelected  && actionDelay > 0)
			{
				actionDelay--;
				floatX-=16;
			
				if (floatX< -floatXMax)
					floatX = -floatXMax;
	
			}
		
		
			else if(isUnSelected && actionDelay > 0){
				actionDelay--;
				floatX+=16;
				if(floatX > 0)
					floatX = 0; 
				
			}
			else if (isUnSelected)
			{
				floatX = 0;
				floatY = 0;

			}
			
			
			
			
			break;
		
		
		}
		alpha = 50;
		if (alphaDelay >0)
		{
			
			
			alphaDelay--;
			 
			if (alphaDelay % 12 ==0)
			{
				if (alpha == 50)
					alpha = 200;
				else alpha = 50;
			}
		
			
			//Globals.logger("new alpha:"+alpha);
		}
		else
			alpha = 225;

		
		
	}
	
	
	
	public  void setSelection(){
		
		
		if(!isSelected){
		//isNewSelection = true;
		isSelected = true;
		isUnSelected = false;
		actionDelay = 16;
	}
		}
	
	
	public void removeSelection()
	{
		if (!isUnSelected)
		{
			isUnSelected = true;
			isSelected = false;
			actionDelay = 16;
			
			
		}
		
		
	}
	
	
	public int movementX(){
		return floatX >> tilePower;
	}
	
	public int movementY(){
		return floatY >> tilePower;
	}
	
	public static void resetEntities(int selectedEntity){
		
	int i=0;
	
		
		
		
		while (i<CombatEntityList.length) {
			if (!CombatEntityList[i].deleted && !CombatEntityList[i].died  ) {
				
				if (selectedEntity == -1  || selectedEntity ==CombatEntityList[i].owner)
				CombatEntityList[i].floatX = 0;
				CombatEntityList[i].floatY = 0;
			
				
			}
			i++;
		}
		
	}
	
	
	
	public static void setFighter(CombatEntity ce,int type,int owner){
		
		int i=0;
		while (i<CombatEntityList.length && !CombatEntityList[i].deleted) i++;
		if (i<CombatEntityList.length) {
			CombatEntityList[i] = ce;
			CombatEntityList[i].init(type, owner);
			//return i;
		}
		
	}
	
	
	public static void  setAttacked(int currentSelection,int mTurn,int dmg){
		
		int i=0;
		int selectionCnt = 0;
		int selection = 0;
		
		if (mTurn == CombatManager.INPLAYERSELECT)
			selection = CombatEntity.ceOWNERPLAYER;
		else if(mTurn == CombatManager.INMONSTERSELECT)
		{
			
			selection = CombatEntity.ceOWNERMONSTER;
		}
		
		
		while (i<CombatEntityList.length) {
			if (!CombatEntityList[i].deleted && !CombatEntityList[i].died && CombatEntityList[i].owner == selection) {
				
				if (selectionCnt == currentSelection)
				{
					CombatEntityList[i].isAttacked(dmg);
					Globals.logger("monster no: "+selectionCnt +" damaged !");
					break;
				
				}
				selectionCnt++;
			}
			i++;
			
		}
		
		
		
		
		
	}
	
	
	private void isAttacked(int damage){
		
		health -= damage;
		alphaDelay = 36;
	
	}

}
