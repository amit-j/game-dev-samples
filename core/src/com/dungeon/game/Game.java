package com.dungeon.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.GLFrameBuffer;
import gameStates.GameState;
import gameStates.PlayState;
import handlers.*;

public class Game extends ApplicationAdapter {
	public static String TITLE = "CODENAME ALLEY";

	public static AssetsLoader res;


	public static FrameBuffer lightBuffer;
	public static FrameBuffer m_fbo;
	TextureRegion lightBufferRegion,m_fboRegion;
	public static final float STEP=1/60f;
	private static float accum;


	public static int gameState = GameState.PLAYSATE;

	public int worldTicks;

	@Override
	public void create() {
		Gdx.input.setInputProcessor(new MyInputProcessor());
		res= new AssetsLoader();
		loadAssets();
		Render.initRender();

	}



	@Override
	public void render() {
		//limiting game at 60fps
		accum+=Gdx.graphics.getDeltaTime();
		while(accum>=STEP){
			accum-=STEP;

			//keep track of frames
			worldTicks++;																// global worldticking
			if (worldTicks>1000) worldTicks=0;
			switch (gameState)
			{

				case GameState.PLAYSATE:
					PlayState.update();
					PlayState.render();

			}
		}
		MyInput.update();

	}



	@Override
	public void resize(int width, int height) {
		//change display size as well as framebuffer to match the changed size

		int displayW=width;
		int displayH=height;
		int h=(int)(displayH/Math.floor(displayH/120));
		int w=(int)(displayW/(displayH/ (displayH/Math.floor(displayH/120)) ));

		//int h=(int)(displayH/(displayW/ (displayW/Math.floor(displayW/120)) ));
		//	int w=(int)(displayW/Math.floor(displayW/160));


		Render.width=w;
		Render.height=h;

		GLFrameBuffer.FrameBufferBuilder frameBufferBuilder = new GLFrameBuffer.FrameBufferBuilder(w,h);
		frameBufferBuilder.addColorTextureAttachment(GL30.GL_RGBA8, GL30.GL_RGBA, GL30.GL_UNSIGNED_BYTE);
		//frameBufferBuilder.addDepthTextureAttachment(GL30.GL_DEPTH_COMPONENT32F, GL30.GL_FLOAT);
		m_fbo = frameBufferBuilder.build();


		Globals.logger("Display size : "+ width +" x "+height + " Pixels :" + w+" x "+ h);
		if (m_fbo!=null) m_fbo.dispose();
		//m_fbo = new FrameBuffer(Pixmap.Format.RGB888, (Render.width), (Render.height), false);
		m_fbo.getColorBufferTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
		m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture(),0,m_fbo.getHeight()-Render.height,Render.width,Render.height);
		m_fboRegion.flip(false, false);


	//	if (lightBuffer!=null) lightBuffer.dispose();


		Render.camera.setToOrtho(true, Render.width,Render.height);
		Globals.logger("Cam :"+Render.camera.position.x + ","+Render.camera.position.y);


	}





	private void loadAssets(){

		res.LoadTexture("m01.png", "m01"); //monster sheet
		res.LoadTexture("t01.png", "t01"); //tile sheet
		res.LoadTexture("uipcfont.png", "ui"); // font and other ui stuff
		res.LoadTexture("blueSheet.png", "gui");
	}


	public static int PowerOf2(int n) {
		int k=1;
		while (k<n) k*=2;
		return k;
	}

}
