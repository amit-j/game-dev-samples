package handlers;

import com.badlogic.gdx.math.Vector2;

public class MyInput {
	
	public static  boolean[] keys;
	public static boolean[] pkeys;
	
	public static final int NUM_KEYS=8;
	public static final int LEFT=0;
	public static final int RIGHT=1;
	public static final int UP=2;
	public static final int DOWN=3;
	public static final int CHANGE=5;

	public static final int ESC=4;
	public static final int SHOOT=6;
	public static final int MOUSE_CLICK=7;
	public static boolean dragOn;
	
	public static Vector2 mousePos;
	public static Vector2 ClickPos;
	public static Vector2 ClickDrag;
	
	static{
		keys=new boolean[NUM_KEYS];
		pkeys=new boolean[NUM_KEYS];
		mousePos=new Vector2();
		ClickPos=new Vector2();
		ClickDrag=new Vector2();
	}
	
	public static void update(){
		for (int i=0;i<NUM_KEYS;i++)
			pkeys[i]=keys[i];
	}
	
	
	public static void setKey(int i,boolean b){keys[i]=b;}
	public static boolean isDown(int i){ return keys[i];}
	public static boolean isPressed(int i){return keys[i] && !pkeys[i];}
	public static Vector2 getMousePos(){ return mousePos;} 

}
