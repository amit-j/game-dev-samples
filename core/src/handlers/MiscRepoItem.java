package handlers;

import com.badlogic.gdx.utils.Array;
import gameStats.Item;

public class MiscRepoItem {
	
	
	public static final int tATTACK = 0,
							tITEM  = 1;

	
	
	Array <Item>item;
	
	public MiscRepoItem(){
		item = new Array<Item>();
	}
	
	public void add(int itemType,int itemID,int count){
		Item i = new Item();
		i.itemType = itemType;
		i.itemID = itemID;
		
		switch(itemType){ // just for the sake of seperating attacks and items
		
		case tATTACK:
			i.initAttack(itemID,count);
			break;
		case tITEM:
			break;
		
		
		}
		item.add(i);
		
		
		
	}
	
	
	
	
	public int getItemCount(){
		return item.size;
	}
	
	
	public Item getItem(int i){
		return item.get(i);
	}
}
	
	
	
	