package handlers;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import entities.Player;

public class World {
	
	public final static int	tileMapW = 100;
	public final static int	tileMapH = 100;

	
	public final static int tile_width =16 ;
	public final static int tile_height =16;
	
	
	public final static int	tEMPTY = 0,
							tFLOOR =1,
							tSOLID = 2,
							tMONSTER=3,
							tMARKER=4;
						
	
	
	//offset for camera position in the world
	public int worldOffsetX;
	public int worldOffsetY;

	//shake the camera, cool for explosions and effects
	int worldShake;
	int worldShakeOffsetX;
	int worldShakeOffsetY;
	
	int			lockScreen;
	boolean		lockVertical;
	boolean		CameraTakeOver;	// if something else besides player takes focus
	boolean		CameraIsView;	// signal player we are back on camera-position checking
	
	int			CameraTakeOverCountdown;
	
	
	int			cameraTargetX;
	int			cameraTargetY;
	
	
	int[] 		tileMap = new int[tileMapW*tileMapH];
	int[]		renderMap = new int[tileMapW*tileMapH];
	
	
	private static final int minCols=3;
	private static final int minRows=3;
	private static final int maxCols=6;
	private static final int maxRows=6;
	private static final int roomCount=8;
	private static final int maxCorridorLen=4;
	
	
	public static Array<Rectangle> rooms;
	
	public int playerStartX;
	public int playerStartY;
	
	/* ===============
	 * initialise the world object
	 * cleans tilemap
	 * ===============
	 */
	public final void initWorld() {
		
		rooms = new Array<Rectangle>();
		for (int x=0; x<tileMapW; x++) 
		{
			for (int y=0; y<tileMapH; y++) 
			{
				put(x,y,tSOLID);
			}
		}
		
		
	}
	
	 /* initialise the world for a new game
	 * ===============
	 */
	public final void initNewGame() {
		CameraTakeOver=false;
		
Array<Rectangle> rooms=new Array<Rectangle>();
		
		//write more comments here. this needs alot of explaining
	 
	 	//drawing the first room
		int rx;
		int ry;
		int rw;
		int rh;
		int flag=0;
		int cnt = 0;
		int dir = 0;
		int cx;
		int cy;
		int cl;
		int inloopCnt=0;
		int lastRoom=-1;
		int lastCorDir=-1;  //why are we using this? 
		
		rw=Globals.getRandomRanged(8,10);
		rh=Globals.getRandomRanged(8,10);
		rx=Math.round(tileMapW/2-rw/2f);
		ry=Math.round(tileMapH/2-rh/2f);
		
		
		playerStartX = rx +rw/2;
		playerStartY = ry+rh/2;
		
		//Gdx.app.log("World", "x: "+rx+" y:"+ry+" w:"+rw+" h:"+rh);
		digFloor(rx, ry, rw, rh);
		Rectangle r=new Rectangle(rx,ry,rw,rh);
		Rectangle c=new Rectangle();
		rooms.add(r);
		
		// Gdx.app.log("World", "x:"+rx+"y:"+ry+"w:"+rw+"h"+rh);
		
		cnt++;
		lastRoom=0;
		flag=0;
		while(cnt<roomCount){
	
		if(flag==0){
		
			dir=Globals.getRandomRanged(0, 3);
			if(dir==(lastCorDir+2)%4)
			{
				while(dir==(lastCorDir+2)%4)
				dir=Globals.getRandomRanged(0, 3);
			}
				lastCorDir=dir;
		}
		else
			if(inloopCnt<3)
			{
				dir=(dir+1)%4;
				inloopCnt++;
				
			}
			else
				return;
		
		
		
		
		switch(dir){
		case Globals.UP:
			Globals.logger("Room Direction : UP");
			//get all the data from old rooms
			rx=(int) rooms.get(lastRoom).x;
			rw=(int)rooms.get(lastRoom).width;
			ry=(int)rooms.get(lastRoom).y;
			rh=(int)rooms.get(lastRoom).height;
			
			
			cx=Globals.getRandomRanged(rx,rx+rw-1);
			
			cl=Globals.getRandomRanged(2,5); //random len corridor
			cy=ry+rh;
			
			rw=Globals.getRandomRanged(8,10);
			rh=Globals.getRandomRanged(8,10);
			rx=Globals.getRandomRanged(cx-rw+1, cx);
			ry=cy+cl;
			
			
		
			
			 r=new Rectangle(rx,ry,rw,rh);
			 c=new Rectangle(cx,cy,1,cl);
			 
			 if(checkOverlap(r) && checkOverlap(c) ){
				
				 rooms.add(r);
				 lastRoom=rooms.size-1;
					
				
				 rooms.add(c);
				 
				 cnt++;
				 flag=0;
				 inloopCnt=0;
				 
			 }
			 
			 else
				flag=1;
			 
			 
			
			
			
			break;
			
		case Globals.DOWN:
			Globals.logger("Room Direction : DOWN");
			rx=(int) rooms.get(lastRoom).x;
			rw=(int)rooms.get(lastRoom).width;
			ry=(int)rooms.get(lastRoom).y;
			rh=(int)rooms.get(lastRoom).height;
			
			cx=Globals.getRandomRanged(rx,rx+rw-1);
			
			cl=Globals.getRandomRanged(2,5); //random len corridor
			cy=ry-cl;
			
			rw=Globals.getRandomRanged(8,10);
			rh=Globals.getRandomRanged(8,10);
			rx=Globals.getRandomRanged(cx-rw+1, cx);
			ry=cy-rh;
			
			
			//TODO : real world values check algo pls
			 r=new Rectangle(rx,ry,rw,rh);
			 c=new Rectangle(cx,cy,1,cl);
			 if(checkOverlap(r) && checkOverlap(c) ){
				
				rooms.add(r);
				lastRoom=rooms.size-1;
				
				rooms.add(c);
				cnt++;
				flag=0;
				inloopCnt=0;
				
			 }
				
			 else
				 flag=1;
			break;
			
		case Globals.RIGHT:
			Globals.logger("Room Direction : RIGHT");
			rx=(int) rooms.get(lastRoom).x;
			rw=(int)rooms.get(lastRoom).width;
			ry=(int)rooms.get(lastRoom).y;
			rh=(int)rooms.get(lastRoom).height;
			
			cx=rx+rw;
			
			cl=Globals.getRandomRanged(2,5); //random len corridor
			cy=Globals.getRandomRanged(ry,ry+rh-1);
			
			rw=Globals.getRandomRanged(8,10);
			rh=Globals.getRandomRanged(8,10);
			ry=Globals.getRandomRanged(cy-rh+1,cy);
			rx=cx+cl;
			
			
			 r=new Rectangle(rx,ry,rw,rh);
			 c=new Rectangle(cx,cy,cl,1);
			 if(checkOverlap(r) && checkOverlap(c) ){
				
				rooms.add(r);
				lastRoom=rooms.size-1;
				
				rooms.add(c);
				cnt++;
				flag=0;
				
				inloopCnt=0;
			 }
			 else
				 flag=1;
			break;
		
		case Globals.LEFT:
			Globals.logger("Room Direction : LEFT");
			rx=(int) rooms.get(lastRoom).x;
			rw=(int)rooms.get(lastRoom).width;
			ry=(int)rooms.get(lastRoom).y;
			rh=(int)rooms.get(lastRoom).height;
			
			
			cl=Globals.getRandomRanged(2,5); //random len corridor
			cx=rx-cl;
			cy=Globals.getRandomRanged(ry,ry+rh-1);
			
			rw=Globals.getRandomRanged(8,10);
			rh=Globals.getRandomRanged(8,10);
			ry=Globals.getRandomRanged(cy-rh+1,cy);
			rx=cx-rw;
			
			
			
			 r=new Rectangle(rx,ry,rw,rh);
			 c=new Rectangle(cx,cy,cl,1);
			 if(checkOverlap(r) && checkOverlap(c) ){
				
				rooms.add(r);
				lastRoom=rooms.size-1;
				
				rooms.add(c);
				cnt++;
				flag=0;
				inloopCnt=0;
				
			 }
			 else
				 flag=1;
			break;
		}
			
		
				
		
		}
		
		
		
		for (int rm = 1; rm< rooms.size; rm++){
			digFloor((int)rooms.get(rm).x,(int) rooms.get(rm).y,(int) rooms.get(rm).width, (int)rooms.get(rm).height);
			
			
		}

	}
	
private  void digFloor(int x,int y,int w,int h){
		Globals.logger("Digging room at x : "+x + " y :"+y+ " with w: "+w+" and height : "+h);
		for(int i=x;i<x+w;i++)
			for(int j=y;j<y+h;j++){
				tileMap[i+(j*tileMapW)]=tEMPTY;
				renderMap[i+(j*tileMapW)] = tFLOOR;
			}
		 
		
	}
	
	private static  boolean checkOverlap(Rectangle r){
		Rectangle n =new Rectangle(r.x-1,r.y-1,r.width+1,r.height+1);
		for(int i=0;i<rooms.size;i++){
			if(rooms.get(i).overlaps(n))
				return false;
		
		}
		return true;
	}
	
	
	
	/* ===============
	 * Clear the specified type from the tilemap (eg: markers)
	 * ===============
	 */
	public final void clearType(int myTile) {
		for (int x=0; x<tileMapW; x++) 
		{
			for (int y=0; y<tileMapH; y++) 
			{
				if (tileMap[x+(y*tileMapW)]==myTile) tileMap[x+(y*tileMapW)]=tEMPTY; 
			}
		}
	}
	
	/* ===============
	 * get specified tile
	 * ===============
	 */
	public final int getTile (int myX, int myY) {
		return tileMap[myX+(myY*tileMapW)];
	}
	
	/* ===============
	 * put specified tile at coords
	 * ===============
	 */
	public final void put (int myX, int myY, int myTile) {
		tileMap[myX+(myY*tileMapW)]=myTile;
	}
	
	/* ===============
	 * put specified tile in whole area
	 * ===============
	 */
	public final void putArea(int myX, int myY, int myW, int myH, int myTile) {
		for (int x=myX; x<myX+myW; x++) 
		{
			for (int y=myY; y<myY+myH; y++) 
			{
				put(x,y,myTile);
			}
		}
	}

	/* ===============
	 * rendermap contains the "Textures" used for every tile in the map
	 * this fetches the specified render tile
	 * ===============
	 */
	public final int getRendermap(int myX, int myY) {
		return renderMap[myX+(myY*tileMapW)];
	}
	
	/* ===============
	 * put specified texture at coords
	 * ===============
	 */
	public final void putRendermap(int myX, int myY, int myTile) {
		renderMap[myX+(myY*tileMapW)]=myTile;
	}
	
	/* ===============
	 * fill area with specified texture
	 * ===============
	 */
	public final void putAreaRendermap(int myX, int myY, int myW, int myH, int myTile) {
		for (int x=myX; x<myX+myW; x++) 
		{
			for (int y=myY; y<myY+myH; y++) 
			{
				putRendermap(x,y,myTile);
			}
		}
	}	
	
	/* ===============
	 * check if specific tile is a "solid" tile (includes monsters)
	 * ===============
	 */
	public final boolean isSolid(int myX, int myY) {
		if (myY<0) return false;
		if (myX<0 || myX>tileMapW || myY>tileMapH) return true;
		if (tileMap[myX+(myY*tileMapW)]>=tSOLID) return true;
		
		return false;
	}
	
	/* ===============
	 * force a camera take over (eg: monster doing something special)
	 * for set duration
	 * ===============
	 */
	public final void setCameraTakeOver(int myx,int myy,int duration) {
		CameraTakeOver=true;
		CameraTakeOverCountdown=duration;
		cameraTargetX=myx;
		cameraTargetY=myy;
	}
	
	public final void setCamera(int x, int y) {
		cameraTargetX=worldOffsetX=x;
		cameraTargetY=worldOffsetY=y;
	}
	
	/* ===============
	 * update camera, centering it on specified player
	 * called every tick
	 * ===============
	 */
	public final void handleCamera(Player tmpPlayer) {
		int tx;
		int ty;

		
		//TODO : check if this speed is good, no idea how this works as of now
		if (CameraTakeOver) 
		{
			int xSpeed=((cameraTargetX- (Render.width>>1)) - worldOffsetX) >> 3;
			int ySpeed=((cameraTargetY- (Render.height>>1)) - worldOffsetY) >> 3;
			
			worldOffsetX+=xSpeed;
			worldOffsetY+=ySpeed;
		} 
		else 
		{
				
			tx=tmpPlayer.x+8;
			ty=tmpPlayer.y+10;
			
			int xSpeed=((tx- (Render.width>>1)) - worldOffsetX) >> 3;
			int ySpeed=((ty- (Render.height>>1)) - worldOffsetY) >> 3;
			
			worldOffsetX+=xSpeed;
			worldOffsetY+=ySpeed;
		}
		
		// correct stuff
		if (worldOffsetX<0) worldOffsetX=0;
		if (worldOffsetX>(tileMapW*tile_width)-Render.width) worldOffsetX=(tileMapW*tile_width)-Render.width;

		if (worldOffsetY<0) worldOffsetY=0;
		if (worldOffsetY>(tileMapH*tile_width)-Render.height) worldOffsetY=(tileMapH*tile_width)-Render.height;
	}	
	 
	
	
	/* ===============
	 * update world every tick
	 * ===============
	 */
	public final void update() {
		if (worldShake>0) worldShake--;
		if (CameraTakeOverCountdown>0) CameraTakeOverCountdown--;
		else CameraTakeOver=false;

		worldShakeOffsetY=0;
		worldShakeOffsetX=0;
		if (worldShake>0) 
		{
			//TODO: Add camera shake to the world 
			/*if (worldShake>24) 
			{
				worldShakeOffsetY=Globals.getRandom(8)-4;
				worldShakeOffsetX=Globals.getRandom(8)-4;
			} 
			else if (worldShake>12) 
			{
				worldShakeOffsetY=Globals.getRandom(4)-2;
				worldShakeOffsetX=Globals.getRandom(4)-2;
			} 
			else 
			{
				worldShakeOffsetY=Globals.getRandom(2)-1;
				worldShakeOffsetX=Globals.getRandom(2)-1;
			}*/
		}	
		
		worldOffsetX+=worldShakeOffsetX;
		worldOffsetY+=worldShakeOffsetY;
	
		
	}
	
	
	
}
