package handlers;

import com.badlogic.gdx.Gdx;

import java.util.Random;

//All the config values as well as static needed functios go here !
//Try to keep this clean :P
public class Globals {
	
	
	
	
	public static final int WIDTH = 320;
	public static final int HEIGHT = 240;
	public static final String TITLE = "DARK ALLEYS";
	
	public static final int UP=0;
	public static final int LEFT=1;
	public static final int DOWN=2;
	public static final int RIGHT=3;
	
	
	//attacks repository

	
	
	public static final int getRandomRanged(float min,float max){
	//Gdx.app.log("random", "min: "+min+" max"+max);
		return (int) (min+(new Random()).nextInt((int) (max-min+1)));
	}
	
	public static final void logger(String message){
		//Gdx.app.log("Dark Debug: ", message);
	}
	
	

}
