package handlers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

public class MyInputProcessor extends InputAdapter{

	public boolean keyDown(int k)
	{
		
		if(k==Keys.LEFT || k==Keys.A)
			MyInput.setKey(MyInput.LEFT, true);
		if(k==Keys.RIGHT || k==Keys.D)
			MyInput.setKey(MyInput.RIGHT, true);
		if(k==Keys.UP ||k==Keys.W)
			MyInput.setKey(MyInput.UP, true);
		if(k==Keys.DOWN || k==Keys.S)
			MyInput.setKey(MyInput.DOWN, true);
		if(k==Keys.ESCAPE)
			MyInput.setKey(MyInput.ESC, true);
		if(k==Keys.C)
			MyInput.setKey(MyInput.CHANGE, true);
		if(k==Keys.X)
			MyInput.setKey(MyInput.SHOOT, true);
		return true;
	}
	public boolean keyUp(int k)
	{
		if(k==Keys.LEFT || k==Keys.A)
			MyInput.setKey(MyInput.LEFT, false);
		if(k==Keys.RIGHT || k==Keys.D)
			MyInput.setKey(MyInput.RIGHT, false);
		if(k==Keys.UP || k==Keys.W)
			MyInput.setKey(MyInput.UP, false);
		if(k==Keys.DOWN || k==Keys.S)
			MyInput.setKey(MyInput.DOWN, false);
		if(k==Keys.ESCAPE)
			MyInput.setKey(MyInput.ESC, false);
		if(k==Keys.C)
			MyInput.setKey(MyInput.CHANGE, false);
		if(k==Keys.X)
			MyInput.setKey(MyInput.SHOOT, false);
		return true;
	}
	
	public boolean mouseMoved(int x,int y){
		MyInput.mousePos.x=x;
		MyInput.mousePos.y=y;
		return true;
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer,int  button){
		
	
		if(button==0){
			MyInput.setKey(MyInput.MOUSE_CLICK, true);
			MyInput.ClickPos.x=x;
			MyInput.ClickPos.y=y;
			return true;
		}
		return false;
	}
  
	
	public boolean touchUp(int x,int y,int pointer,int button){
		
		if(button==0){
			MyInput.setKey(MyInput.MOUSE_CLICK, false);
			MyInput.ClickPos.x=x;
			MyInput.ClickPos.y=y;
			MyInput.dragOn=false;
		}
		return true;
		
	}

	public boolean touchDragged(int x,int y,int pt){
		MyInput.ClickDrag.x=x;
		MyInput.ClickDrag.y=y;
		MyInput.dragOn=true;
		return true;
	}
}
