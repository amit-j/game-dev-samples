package handlers;


import com.dungeon.game.Game;
import combatEntities.CombatEntity;
import entities.Player;
import gui.SelectionMenu;

public class CombatManager {

	public static final int INITCOMBAT =0,
							INACTIONSELECT = 1,
							INPLAYERSELECT = 2,
							INABILITYSELECT = 3,
							INMONSTERSELECT  = 4,
							MONSTERTURN = 5,
							COMBATOVER = 6;
	
	
	
	public static int combatState ;
	
	
	private static final int[] playerArenaSpotY = {1,2,3,4,5};
	private static final int[] monsterArenaSpotY = {1,2,3,4,5};
	private static final int monsterArenaSpotX = Render.width - 40;

	
	int playerCount;
	static int monsterCount;
	
	static int currentSelection = 0;
	
	static int selectedPlayer = -1;
	static int selectedMonster  = -1;
	static boolean isActiveWindow = true;
	static int cnt;
	
	
	private static SelectionMenu menuPLAYERACTION;
	
	
	
	
	public static void updateCombat(Player mPlayer){
		handleInput(mPlayer);
		switch(combatState){
		
		case INITCOMBAT:
			Globals.logger("Init Combat state");
			CombatEntity.killAll();
			monsterCount = 4;
			setFighters(mPlayer);
			switchGameState(INACTIONSELECT);

			break;
			
		case INACTIONSELECT:
			
			
			break;
		case INPLAYERSELECT:
			updateCombatEntities(currentSelection);
			break;
			
		case INABILITYSELECT:
			menuPLAYERACTION.update();
		case INMONSTERSELECT:
			updateCombatEntities(currentSelection);
			break;
		case MONSTERTURN:
			
			
			
			if (!isActiveWindow){menuPLAYERACTION.update();
			if(!menuPLAYERACTION.isActive){
				isActiveWindow = true;
			}
			
			}
			
			
			break;

			
		}
	}
	
	
	
	public static void renderCombat(Player mPlayer){
		
		int tx;
		int ty;
		
		
		
		int i=0;
		CombatEntity tmpMonster;
		int tx2,ty2; //used to draw monsters 
		tx = 10;
		ty = 10;
		tx2 = Render.width - 20;
		ty2= 10;
		
		
		
		//draw action selection menu
		
		
		while (i<CombatEntity.CombatEntityList.length) 
		{
			tmpMonster=CombatEntity.CombatEntityList[i];
			
			if (!tmpMonster.deleted )
					//&& !tmpMonster.died ) 
			{
				Render.setAlpha(tmpMonster.alpha);
				if (tmpMonster.owner == CombatEntity.ceOWNERPLAYER){

					Render.dest.set(tx + tmpMonster.movementX(),ty+tmpMonster.movementY(),tx+tmpMonster.movementX()+tmpMonster.w,ty+tmpMonster.movementY()+tmpMonster.h);
					Render.src.set(tmpMonster.xOffset, tmpMonster.yOffset, tmpMonster.xOffset+tmpMonster.w, tmpMonster.yOffset+tmpMonster.h);
					Render.drawBitmap(Game.res.getTexture("m01"));
					//tx += 10;
					ty+= 20;
				}
				else{
					
					Render.dest.set(tx2 + tmpMonster.movementX(),ty2+tmpMonster.movementY(),tx2+tmpMonster.movementX()+tmpMonster.w,ty2+tmpMonster.movementY()+tmpMonster.h);
					Render.src.set(tmpMonster.xOffset, tmpMonster.yOffset, tmpMonster.xOffset+tmpMonster.w, tmpMonster.yOffset+tmpMonster.h);
					Render.drawBitmap(Game.res.getTexture("m01"));
					ty2+=20;
				}
			}
			i++;
		}
		Render.setAlpha(225);
		//draw action selection menu
		Render.dest.set(5,Render.height -35,Render.width - 5,Render.height - 5);
		Render.src.set(0, 192, 189, 236);
		Render.drawBitmap(Game.res.getTexture("gui"));
		
		
		
		switch (combatState)
		{
		
		case INACTIONSELECT:
			if (currentSelection == 0)
			{
				Render.setAlpha(255);
				Render.dest.set(10,Render.height - 31, 15, Render.height - 26);
				Render.src.set(60, 239, 69, 253);
				Render.drawBitmap(Game.res.getTexture("gui"));
			GUI.renderText("FIGHT",0, 18, Render.height - 31, 100, 0, 2);
			}
			else
			{
				Render.setAlpha(170);
				GUI.renderText("FIGHT",0, 10, Render.height - 31, 100, 0, 2);
			
			}
			
			if (currentSelection == 1)
			{
				Render.setAlpha(255);
				Render.dest.set(Render.width - 50,Render.height - 31, Render.width - 45, Render.height - 26);
				Render.src.set(60, 239, 69, 253);
				Render.drawBitmap(Game.res.getTexture("gui"));
				GUI.renderText("ITEMS",0, Render.width - 42, Render.height - 31, 100, 0, 2);
			}
			else
			{
				Render.setAlpha(170);
				GUI.renderText("ITEMS",0, Render.width - 50, Render.height - 31, 100, 0, 2);
			
			}
			
			
			if (currentSelection == 2)
			{
				Render.setAlpha(255);
				Render.dest.set(10,Render.height - 15, 15, Render.height - 10);
				Render.src.set(60, 239, 69, 253);
				Render.drawBitmap(Game.res.getTexture("gui"));
				GUI.renderText("BACKUP",0, 18, Render.height - 15, 100, 0, 2);
			}
			else
			{
				Render.setAlpha(170);
				GUI.renderText("BACKUP",0, 10, Render.height - 15, 100, 0, 2);
			
			}
			
			

			if (currentSelection == 3)
			{
				Render.setAlpha(255);
				Render.dest.set(Render.width - 50,Render.height - 15, Render.width - 45, Render.height - 10);
				Render.src.set(60, 239, 69, 253);
				Render.drawBitmap(Game.res.getTexture("gui"));
				GUI.renderText("ESACPE",0, Render.width - 42, Render.height - 15, 100, 0, 2);
			}
			else
			{
				Render.setAlpha(170);
				GUI.renderText("ESACPE",0, Render.width - 50, Render.height - 15, 100, 0, 2);
			
			}
			
			
			Render.setAlpha(255);
			break;
		case INPLAYERSELECT:
			GUI.renderText("Select your fighter!",0, 10, Render.height - 31, 2000, 0, 2);
		
			break;
			
		case INMONSTERSELECT:
			GUI.renderText("Who do I KILL!?!!",0, 10, Render.height - 31, 2000, 0, 2);
		
			break;
		case INABILITYSELECT:
			GUI.renderText("What do i do..?!",0, 10, Render.height - 31, 2000, 0, 2);
			menuPLAYERACTION.render();
			break;
		case MONSTERTURN:
			
			
			
		
			
			
			break;
		
		}
		
	}
	
	
	private static void setFighters(Player mPlayer){

		for ( int iPlayer = 0; iPlayer < mPlayer.teamCount; iPlayer++)
		{

			
			CombatEntity.setFighter(mPlayer.team[iPlayer],mPlayer.team[iPlayer].type,CombatEntity.ceOWNERPLAYER);
			
		}
		
		
		for (int iMonster = 0 ;iMonster < monsterCount -1 ;iMonster++){
			
			int tx;
			int ty;
			tx= monsterArenaSpotX;  //* World.tile_width;
			ty=monsterArenaSpotY[iMonster]  * World.tile_height;
			
			CombatEntity.addFighter(tx, ty, CombatEntity.ceMONSTER, CombatEntity.ceOWNERMONSTER);
		}
	
		
	}
	
	
	private static void handleInput(Player mPlayer){
		
		switch(combatState){
		
		
		case INACTIONSELECT:
			if(MyInput.isPressed(MyInput.UP)){
				currentSelection-=2;	
				currentSelection = currentSelection %  4 ;
				
				if (currentSelection < 0 )
					currentSelection =  4 + currentSelection;
				
			}
			else if(MyInput.isPressed(MyInput.DOWN)){
				currentSelection+=2;
				currentSelection = currentSelection %  4 ;
				
			}
			else if(MyInput.isPressed(MyInput.LEFT)){
				currentSelection--;
				currentSelection = currentSelection %  4 ;
				if (currentSelection < 0 )
					currentSelection =  4 + currentSelection;
				
			}
			else if(MyInput.isPressed(MyInput.RIGHT)){
				currentSelection++;
				currentSelection = currentSelection %  4 ;
				
			}
			else if (MyInput.isPressed(MyInput.SHOOT)){
				switchGameState(INPLAYERSELECT);
				
				
			}
			break;
		case INPLAYERSELECT:
			cnt = mPlayer.teamCount;
				
			if(MyInput.isPressed(MyInput.UP)){
				currentSelection--;	
				currentSelection = currentSelection %  mPlayer.teamCount ;
				
				if (currentSelection < 0 )
					currentSelection =  mPlayer.teamCount + currentSelection;
				
			}
			else if(MyInput.isPressed(MyInput.DOWN)){
				currentSelection++;
				currentSelection = currentSelection %  mPlayer.teamCount ;
				
			}
			else if (MyInput.isPressed(MyInput.SHOOT)){
				menuPLAYERACTION = new SelectionMenu(50,playerArenaSpotY[currentSelection]  * World.tile_height,55,33,3, new SelectionMenu.FadeListener(){
					public void onEscape(){
						Globals.logger("called!");
						switchGameState(INPLAYERSELECT);
						currentSelection = 0;
					}
					
				
					public void onDone(){
						Globals.logger("in monster selct");
						switchGameState(INMONSTERSELECT);
						currentSelection = 0;				}
				}
				,mPlayer.team[currentSelection].mAttacks); 
				selectedPlayer = currentSelection;
				Globals.logger("selection for player set at "+ selectedPlayer);
				switchGameState(INABILITYSELECT);
				
			}
			else if(MyInput.isPressed(MyInput.ESC)){
				CombatEntity.resetEntities(-1);
				switchGameState(INACTIONSELECT);
				currentSelection = 0;
			}
			break;
			
			
			
			
		case INMONSTERSELECT:
			
			cnt = mPlayer.teamCount;
			if(MyInput.isPressed(MyInput.UP)){
				currentSelection--;	
				currentSelection = currentSelection %  mPlayer.teamCount ;
				
				if (currentSelection < 0 )
					currentSelection =  mPlayer.teamCount + currentSelection;
				
			}
			else if(MyInput.isPressed(MyInput.DOWN)){
				currentSelection++;
				currentSelection = currentSelection %  mPlayer.teamCount ;
				
			}
			else if (MyInput.isPressed(MyInput.SHOOT)){
			CombatEntity.setAttacked(currentSelection, combatState, 1);
			switchGameState(INPLAYERSELECT);
			
			currentSelection = 0;
				
			}
			else if(MyInput.isPressed(MyInput.ESC)){
				CombatEntity.resetEntities(CombatEntity.ceMONSTER);
				currentSelection = selectedPlayer;
				
				menuPLAYERACTION = new SelectionMenu(50,playerArenaSpotY[currentSelection]  * World.tile_height,55,33,3, new SelectionMenu.FadeListener(){
					public void onEscape(){
						CombatEntity.resetEntities(-1);
						switchGameState(INPLAYERSELECT);
						currentSelection = 0;
					}
					
				
					public void onDone(){
						CombatEntity.resetEntities(-1);
				
						switchGameState(INPLAYERSELECT);
						currentSelection = 0;				}
				}
				,mPlayer.team[currentSelection].mAttacks); 
				switchGameState(INABILITYSELECT);
				
			}
			break;
		}
		
		
	}
	
	
	
	private static void updateCombatEntities(int currentSelection){
		
		CombatEntity.updateCombatEntity(currentSelection,combatState);
		
}
	
	private static void switchGameState(int newGameState){
		combatState = newGameState;
	
		switch(combatState){
		case INABILITYSELECT:
			Globals.logger("Game State Changed to : Ability Select");
			break;
		case INACTIONSELECT:
			Globals.logger("Game State Changed to : Action Select");
			break;
		case INITCOMBAT:
			Globals.logger("Game State Changed to : Init Combat");
			break;
		case INMONSTERSELECT:
			Globals.logger("Game State Changed to : Monster Select");
			break;
		case INPLAYERSELECT:
			Globals.logger("Game State Changed to : Player Select");
			break;
		
		}
		
		
	}
	
	
	
	
}
