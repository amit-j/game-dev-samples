package gameStates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.dungeon.game.Game;
import combatEntities.CombatEntity;
import entities.Bullets;
import entities.FX;
import entities.Monsters;
import entities.Player;
import handlers.*;

public class PlayState extends GameState {
	
	
	//Gamestate when actual gameplay starts
	
	public static final  int INITMAP = 0 ,
							 INITGAME =1,
							 INGAME = 2,
							 INCOMBAT =3,
							 INDEBUG=99;




	public static int state  = INITGAME;
	
	
	static World myWorld=new World();
	static Player myPlayer = new Player();
	
	private static final BitmapFont font = new BitmapFont(true);
	
	public static void render(){
		
		
		Gdx.gl.glDisable(GL20.GL_BLEND);
    	Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
    	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    	Render.camera.update();
        
    	Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    	Render.setAlpha(255);
    	
    	
		Render.drawPaint(255, 0, 0, 0);
		Render.batch.setProjectionMatrix(Render.camera.combined);
		Render.batch.begin();
		//draw world
		//Render.drawPaint(255, 0, 0, 0);
		
		
		
		switch (state)
		{
		case INGAME:
			renderScene();
			//renderPlayer(myPlayer);
			GUI.renderText("Day : 1",0,-1,32,116,0,2);
			GUI.renderBar(10, 10, 90, 100);
			
			font.draw(Render.batch,"x :"+myPlayer.x, 0, 0);
			font.draw(Render.batch,"y :"+myPlayer.y, 0, 50);
			
			break;
		case INDEBUG:
			for (int i = 0;i< World.rooms.size -1;i++)
				Render.drawRect(World.rooms.get(i).x, World.rooms.get(i).y, World.rooms.get(i).width, World.rooms.get(i).height);
			break;
		
			
			
		case INCOMBAT:
			CombatManager.renderCombat(myPlayer);
			
			break;
		}
		
		Render.batch.end();
	
	}
	
	
	public static void update(){
		
		
		switch(state){
		
		case INITGAME:
			Monsters.initMonsters();
			Bullets.initBullets();
			CombatEntity.initCombatEntity();
			FX.initFX();
			GUI.initGui("uipcfont.png");
			state = INITMAP;
			break;
		
		case INITMAP:
			Monsters.killAll();
			FX.killAll();
			Bullets.killAll();

			myWorld.initWorld();
			myWorld.initNewGame();
			myPlayer.init( myWorld.playerStartX *World.tile_width, myWorld.playerStartY*World.tile_height);
			
			
			state = INCOMBAT;
			Globals.logger("Inititalized playState");
			break;
		
		case INGAME:
			
			myPlayer.update(myWorld);
			myWorld.update();
			myWorld.handleCamera(myPlayer);
			
			Monsters.updateMonsters(myWorld, myPlayer);
			Bullets.updateBullets(myWorld, myPlayer);
			FX.updateFX(myWorld,myPlayer);
			break;
			
		case INCOMBAT:
			
			CombatManager.updateCombat(myPlayer);
			break;
			
			
			
		}
			
			
			
			
			
		}
	
	
	private static void renderScene(){
		int tx;
		int ty;
		
		tx= -myWorld.worldOffsetX;
		for(int x =0; x < Render.width; x++)
		{
			
			if(tx> -World.tile_width && tx < Render.width)
			{
				
				
				ty=-myWorld.worldOffsetY;
				for (int y=0; y < World.tileMapH; y++) {
					
					if (ty>-World.tile_height && ty<Render.height) 
					{
						
							switch(myWorld.getRendermap(x, y))
							{
							
							case World.tFLOOR: //drawing for test!
								
								Render.dest.set(tx, ty, tx + World.tile_width, ty + World.tile_height);
								Render.src.set(0, 81, 12, 93);
								Render.drawBitmap(Game.res.getTexture("t01"));
								break;
							
							/*case World.tSOLID:
								Render.dest.set(tx, ty, tx + World.tile_width, ty + World.tile_height);
								Render.src.set(0, 68, 16, 80);
								Render.drawBitmap(Game.res.getTexture("m01"));*/
							//break;
							
							}
							
							
						
					}
					ty+=World.tile_height;
					
				}
					
			}
			tx+=World.tile_width;
		}
		
		
		renderMonsters(1);
		renderPlayer(myPlayer);
		renderBullets(1);
		renderFX(1);
		
		
	}
	
	public static final void renderPlayer( Player tmpPlayer ) {
		if (!tmpPlayer.visible) return;
		
		int tx;
		int ty;
		tx=tmpPlayer.x-myWorld.worldOffsetX;
		ty=tmpPlayer.y-myWorld.worldOffsetY;
		// player sprite
		Render.dest.set(tx,ty,tx+10,ty+10);
		Render.src.set(tmpPlayer.xOffset, tmpPlayer.yOffset, tmpPlayer.xOffset+tmpPlayer.w, tmpPlayer.yOffset+tmpPlayer.h);
		Render.drawBitmap(Game.res.getTexture("m01"));
		
	}
	
	
	/* ===============
	 * render bullets entities
	 * you can use myRenderPassID to just render 
	 * those entities with renderPass set to that value
	 * ===============
	 */
	public final static void renderBullets(int myRenderPassID) {
		int i=0;
		int tx;
		int ty;
		
		Bullets tmpBullet;
		
		while (i<Bullets.bulletList.length) 
		{
			tmpBullet=Bullets.bulletList[i];
			
			if (!tmpBullet.deleted 
					&& !tmpBullet.died 
					&& tmpBullet.visible 
					&& tmpBullet.renderPass==myRenderPassID) 
			{
					tx=tmpBullet.x-myWorld.worldOffsetX;
					ty=tmpBullet.y-myWorld.worldOffsetY;
					
					Render.dest.set(tx,ty,tx+tmpBullet.w, ty+tmpBullet.h);
					Render.src.set(tmpBullet.xOffset, tmpBullet.yOffset, tmpBullet.xOffset+tmpBullet.w, tmpBullet.yOffset+tmpBullet.h);
					Render.drawBitmap(Game.res.getTexture("m01"));
					
			}
			i++;
		}
	}	
	
	
	/* ===============
	 * render FX entities
	 * you can use myRenderPassID to just render 
	 * those entities with renderPass set to that value
	 * ===============
	 */
	public final static void renderFX(int myRenderPassID) {
		int i=0;
		int tx;
		int ty;
		
		FX tmpFX;
		
		while (i<FX.fxList.length) 
		{
			tmpFX=FX.fxList[i];
			
			if (!tmpFX.deleted 
					&& !tmpFX.died 
					&& tmpFX.visible 
					&& tmpFX.renderPass==myRenderPassID) 
			{
				tx=tmpFX.x-myWorld.worldOffsetX;
				ty=tmpFX.y-myWorld.worldOffsetY;
				
				Render.setAlpha(tmpFX.alpha);
				if (tmpFX.myType==FX.fSPEECH) {
				//	GUI.renderText(Globals.dudeQuotes[tmpFX.subType],0,tx,ty,180,0,0);
				} else if (tmpFX.myType==FX.fSCOREPLUME) {
					//GUI.renderText(Integer.toString(tmpFX.subType),0,tx,ty,180,0,6);
				} else {
					Render.dest.set(tx,ty,tx+tmpFX.w, ty+tmpFX.h);
					Render.src.set(tmpFX.xOffset, tmpFX.yOffset, tmpFX.xOffset+tmpFX.w, tmpFX.yOffset+tmpFX.h);
					Render.drawBitmap(Game.res.getTexture("m01"));
				}
			}
			i++;
		}
		
		Render.setAlpha(255);
	}		
	
	
	
	/* ===============
	 * render monster entities
	 * you can use myRenderPassID to just render 
	 * those entities with renderPass set to that value
	 * ===============
	 */
	public final static void renderMonsters(int myRenderPassID) {

		int i=0;
		int tx;
		int ty;
		
		Monsters tmpMonster;
		
		while (i<Monsters.monsterList.length) 
		{
			tmpMonster=Monsters.monsterList[i];
			
			if (!tmpMonster.deleted 
					&& !tmpMonster.died 
					&& tmpMonster.visible 
					&& tmpMonster.renderPass==myRenderPassID) 
			{
					tx=tmpMonster.x-myWorld.worldOffsetX;
					ty=tmpMonster.y-myWorld.worldOffsetY;
					
					Render.dest.set(tx,ty,tx+tmpMonster.w, ty+tmpMonster.h);
					Render.src.set(tmpMonster.xOffset, tmpMonster.yOffset, tmpMonster.xOffset+tmpMonster.w, tmpMonster.yOffset+tmpMonster.h);
					Render.drawBitmap(Game.res.getTexture("m01"));
					
			}
			i++;
		}
	}
	
	
	private static void renderCombatScene()
	{
		
		
		
		
		
		
		
		
	}
	
	
	private void placePlayersForCombat(){
		
	
		
	}
}
