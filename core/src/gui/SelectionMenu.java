package gui;


import com.dungeon.game.Game;
import handlers.*;

public class SelectionMenu {
	
	

	

						 
	
	int alpha;
	  int globalRed;
	  int globalGreen;
	  int globalBlue;	
	
	public int currentSelection;
	int itemCount;
	
	
	
	FadeListener fade;
	int x;
	int y;
	int width;
	int height;
	
	//boolean isVisible;
	public boolean isActive;
	public int actionDelay; //to prevent accidental clicks
	
	
	String[] items;
	
	public SelectionMenu(int tx,int ty,int w,int h,int cnt,FadeListener fadeListener, MiscRepoItem mItems){
		x=tx;
		y=ty;
		width =w;
		height = h;
		//defaul values
		alpha = 1;
		globalRed = 46;
		globalGreen = 67;
		globalBlue = 91;
		fade = fadeListener;
		actionDelay = 4;
		itemCount = mItems.getItemCount();
		
		//isVisible = true;
		isActive = true;
		
		items = new String[cnt];
		
		for (int i =  0 ; i < mItems.getItemCount()   ;i++){
			items[i] = mItems.getItem(i).title;
		}
	
		
		Globals.logger("new menu create:  " + x + ","+y+"w"+w+"h"+h);
	}
	
	
	
	public void update(){
		if (actionDelay > 0)
			actionDelay--;
		else
			handleInput();
	}
	
	
	public void render(){
		
		if(isActive){
		
		
		
	//	Render.setAlpha(50);
	Render.dest.set(x,y, x+width, y+height);
		Render.src.set(190, 95, 289, 193);
		Render.drawBitmap(Game.res.getTexture("gui"));
		//Render.fillRect(x,y, x+width, y+height);
		
		int ty = y + 5;
		
		for(int i = 0 ; i <itemCount  ; i ++){
			if (i == currentSelection ){
				Render.setAlpha(255);
				Render.dest.set(x+2,ty, x+7, ty+5);
				Render.src.set(60, 239, 69, 253);
				Render.drawBitmap(Game.res.getTexture("gui"));
				GUI.renderText(items[i], 0, x +10, ty, 50, 0, 2);
				
			}
			else{
				Render.setAlpha(170);
			GUI.renderText(items[i], 0, x +5, ty, 50, 0, 2);
			
		}
			ty+=10;
		}		
		Render.setAlpha(255);
		
	}}
	
	
	private void handleInput(){
		
		
		if(isActive){
			
			
				
				
				if(MyInput.isPressed(MyInput.UP)){
					currentSelection--;	
					currentSelection = currentSelection %  itemCount ;
					
					if (currentSelection < 0 )
						currentSelection =  itemCount + currentSelection;
					
				}
				else if(MyInput.isPressed(MyInput.DOWN)){
					currentSelection++;
					currentSelection = currentSelection %  itemCount ;
					
				}
				else if(MyInput.isPressed(MyInput.ESC)){
						isActive = false;
						
						fade.onEscape();
						
				}
				else if(MyInput.isPressed(MyInput.SHOOT)){
					isActive = false;
					fade.onDone();
					
			}
				
			
		}
		
		
	
	}
	
	 public static class FadeListener
	 /* 4522:     */   {
	 /* 4523:     */     public void onDone() {}
	 					 public void onEscape(){}
	 /* 4524:     */   }
}
