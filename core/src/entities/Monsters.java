package entities;

import combatEntities.CombatEntity;
import handlers.Globals;
import handlers.Render;
import handlers.World;

public class Monsters extends Entity {
	
	public static Monsters[] monsterList=new Monsters[320];
	
	int		floatX;
	int		floatY;
	int		xSpeed;
	int		ySpeed;
	int		ySpeedIncrease;
	int		myDirection;
	int		startX;
	int		startY;
	int		targetX;
	int		targetY;
	
	int		xOffsetAdd;
	int		animationDelay;
	int		actionDelay;
	
	
	public boolean visible;
	boolean	activated;
	int		energy;
	int		subType;
	int		myType;
	int		aiState;
	int		aiCountdown;
	int		fireDelay;
	CombatEntity[] team;
	public int renderPass;
	
	public final static void initMonsters() {
		for (int i=monsterList.length - 1; i >= 0; i--) monsterList[i]=new Monsters();
		killAll();
	}
	
	
	public final static void killAll() {
		for (int i=monsterList.length - 1; i >= 0; i--) monsterList[i].deleted=true;
	}
	
	
	public final static int addMonster(int mType, int myX, int myY, int mSubType, World myWorld) {
		int i=0;
		while (i<monsterList.length && !monsterList[i].deleted) i++;
		if (i<monsterList.length) {
			monsterList[i].init(mType, myX, myY, mSubType, myWorld);
			return i;
		}
		return -1;
	}
	
	
	public final void init(int mType, int myX, int myY, int mSubType, World myWorld) {
		deleted=false;
		died=false;
		
		subType=mSubType;
		myType=mType;
		aiState=0;

		visible=true;
		
		activated=false;
		
		x=myX;
		y=myY;
		
		fireDelay=0;
		
		switch (myType) {
		
		}
		
		team = new CombatEntity[5];
		floatX=x<<tilePower;
		floatY=y<<tilePower;
	}
	
	
	
	
	
	public final void update(World myWorld, Player myPlayer) {
		boolean onScreen;
		
		if (x+w>myWorld.worldOffsetX-16 && x<myWorld.worldOffsetX+ Render.width+16
			&& y+h>myWorld.worldOffsetY-16 && y<myWorld.worldOffsetY+Render.height+16) onScreen=true;
		else onScreen=false;
		
		//if (!onScreen && myType!=mSHIP) return;
		
		
		boolean hitPlayer=false;
		if (myPlayer.x+8>=x && myPlayer.x+1<x+w && myPlayer.y+8>=y && myPlayer.y+1<y+h) hitPlayer=true;
		
		switch (myType) {
		}
		
	}
	
	
	
	
	
	public final static void updateMonsters(World myWorld, Player myPlayer) {
		int i=0;
		
		while (i<monsterList.length) {
			if (!monsterList[i].deleted && !monsterList[i].died) {
				monsterList[i].update(myWorld,myPlayer);
				
				if (monsterList[i].died) {
					// monster died
					monsterList[i].deleted=true;
				}
				
			}
			i++;
		}
	}
	
	
	
	public final boolean playerInOurSight(Player myPlayer) {
		if (myPlayer.y+10>y-20 && myPlayer.y<y+20) 
		{
			if (myPlayer.x+5>x && myDirection== Globals.RIGHT) return true;
			if (myPlayer.x+5<x && myDirection==Globals.LEFT) return true;
		}
		
		return false;
	}
	

	public final boolean hit(Bullets myBullet, World myWorld, Player myPlayer) {
		boolean result=false;
		
		
		switch (myType) {
		}
		
		return result;
	}
	
	
private void doMovement(World myWorld){
		
		int tx;
		int ty;
		int tx2;
		int ty2;
		floatX+=xSpeed;
		x=floatX>>tilePower;
			
			if (x<myWorld.worldOffsetX) 
			{
				x=myWorld.worldOffsetX;
				floatX=x<<tilePower;
			}
			
			
			ty= (y+ h/4) >> tilePower; //tile for y
			ty2	= (y + h -2) >> tilePower;
			//collison checks
				
				if (xSpeed > 0) //check right!
				{
					tx = (x+w) >> tilePower;
					if (myWorld.isSolid(tx, ty) || myWorld.isSolid(tx, ty2) ){
						x = (tx << tilePower) -w;
						xSpeed = 0;
						floatX = x << tilePower;
					}
					
				}
				else if (xSpeed < 0)		//left
				{
					tx=(x)>>tilePower;
					if (myWorld.isSolid(tx, ty)  || myWorld.isSolid(tx, ty2) ) 
					{
						x=(tx<<tilePower)+World.tile_width;
						floatX=x<<tilePower;
						xSpeed=0;
					}
				}
				
				floatY+=ySpeed;
				y=floatY>>tilePower;
						
				tx = (x+w - 2) >> tilePower;
				tx2 = x >> tilePower;		
				if (ySpeed < 0) //check up
				{
					ty=(y)>>tilePower;
						if (myWorld.isSolid(tx, ty) || myWorld.isSolid(tx2, ty)) 
							{
								y=(ty<<tilePower)+World.tile_height;
								floatY=y<<tilePower;
								ySpeed=0;
								
							}
					
				}
				else if (ySpeed >  0){
					
					
					ty=(y+h)>>tilePower;
						if (myWorld.isSolid(tx, ty)|| myWorld.isSolid(tx2, ty) ) 
						{
							y=(ty<<tilePower)-(h-1); 	
							floatY=y<<tilePower;
							ySpeed = 0;
						}
				}
				
			
			
	}	


public int addMemeber(){
	int i=0;
	while (i<team.length && !team[i].deleted) i++;
	if (i<team.length) {
		
		return i;
	}
	return -1;
	
}

	
	

}
