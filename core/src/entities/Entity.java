package entities;

/**
 * Base Entity class, monster, bullets, fx ,players, all extend from this
 * 
 * @author Amit
 *
 */
public class Entity {

	public int		x;
	public int		y;
	public int		xOffset;
	public int		yOffset;
	public int		w;
	public int		h;
	public int		rotation;
	public int		alpha;
	
	public boolean	died;
	public boolean	deleted;
	
	
	public int tilePower = 4;
	
}