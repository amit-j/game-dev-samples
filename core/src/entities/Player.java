package entities;

import combatEntities.CombatEntity;
import handlers.Globals;
import handlers.MyInput;
import handlers.World;

public class Player extends Entity{
	
	// position
		public int		xSpeed;
		public int		ySpeed;
		public int		myDirection;
		
		int		floatX;
		int		floatY;
		
		// stats
		int		health;
		int		score;
		int		creditsCollected;
		int		healthDelay;
		
		// appearance
		int		xOffsetAdd;
		int		animationDelay;
		int		actionDelay;  //shootin delay
		public boolean visible;
		
		int tilePower = 4;  //2^4 = 16;
		
		
		//movement
		
		int maxSpeed = 12;
		int speedAdd =2;
		
		//team ? 
		public int teamCount = 3;
		int[] teamMemberCode ;
		int maxTeamCount = 5;
		
		public CombatEntity[] team;
		/* ===============
		 * initialise this player for a new game (resetting score, health ,etc)
		 * ===============
		 */
		public final void newGameInit() {
			creditsCollected=0;
			score=0;
			health=9;
			
			
			
		}
		
		
		
		/* ===============
		 * initialise the player for a new level
		 * at specific starting coordinates
		 * ===============
		 */
		public final void init(int startX, int startY) {
			x=startX;
			y=startY;
			
			myDirection= Globals.UP;
			
			xOffset=0;
			yOffset=0;
			xOffsetAdd=10;
			w=10;
			h=10;
			xSpeed=0;
			ySpeed=0;
			
			actionDelay=0;
		
		
			visible = true;
			floatX=x<<tilePower;
			floatY=y<<tilePower;
			healthDelay=0;
			
		team = new CombatEntity[5];

		for (int i = 0; i < team.length - 2; i++) {
			team[i] =  new CombatEntity();
			team[i].deleted = true;
			
			if (i % 2 == 1)
				team[i].type = CombatEntity.ceMONSTER;
			else
				team[i].type = CombatEntity.ceROBOT;
			team[i].owner = CombatEntity.ceOWNERPLAYER;
			team[i].deleted = false;
			team[i].died = false;
			
			team[i].alpha = 225;
		}
		
		
		}
			
			
			






			public void update(World myWorld){


				if (!visible) return;

				if (healthDelay>0) healthDelay--;
			if (MyInput.isDown(MyInput.LEFT))
			{
				
				if (xSpeed>-maxSpeed) xSpeed-=speedAdd;
				else xSpeed=-maxSpeed;
				
				myDirection=Globals.LEFT;
			}
			else if (MyInput.isDown(MyInput.RIGHT))
			{
				
				if (xSpeed<maxSpeed) xSpeed+=speedAdd;
				else xSpeed=maxSpeed;
				
				myDirection=Globals.RIGHT;
			}
			else{

				if (xSpeed<0) 
				{
					xSpeed+=2;
					if (xSpeed>=0) xSpeed=0;
				} 
				else if (xSpeed>0) 
				{
					xSpeed-=2;
					if (xSpeed<=0) xSpeed=0;
				}
			}
			 if (MyInput.isDown(MyInput.UP))
			{
				
				if (ySpeed > -maxSpeed) ySpeed-=speedAdd;
				else ySpeed= - maxSpeed;
				
				myDirection=Globals.UP;
			}
			else if (MyInput.isDown(MyInput.DOWN))
			{
				
				if (ySpeed < maxSpeed) ySpeed+=speedAdd;
				else ySpeed=maxSpeed;
				
				myDirection=Globals.DOWN;
			}
			else{
				

				if (ySpeed<0) 
				{
					ySpeed+=2;
					if (ySpeed>=0) ySpeed=0;
				} 
				else if (ySpeed>0) 
				{
					ySpeed-=2;
					if (ySpeed<=0) ySpeed=0;
				}
			}
			doMovement(myWorld);
			animate();
				
				
			
		}
		
	private void animate(){
		
	}
	private void doMovement(World myWorld){
		
		int tx;
		int ty;
		int tx2;
		int ty2;
		floatX+=xSpeed;
		x=floatX>>tilePower;
			
			if (x<myWorld.worldOffsetX) 
			{
				x=myWorld.worldOffsetX;
				floatX=x<<tilePower;
			}
			
			
			ty= (y+ h/4) >> tilePower; //tile for y
			ty2	= (y + h -2) >> tilePower;
			//collison checks
				
				if (xSpeed > 0) //check right!
				{
					tx = (x+w) >> tilePower;
					if (myWorld.isSolid(tx, ty) || myWorld.isSolid(tx, ty2) ){
						x = (tx << tilePower) -w;
						xSpeed = 0;
						floatX = x << tilePower;
					}
					
				}
				else if (xSpeed < 0)		//left
				{
					tx=(x)>>tilePower;
					if (myWorld.isSolid(tx, ty)  || myWorld.isSolid(tx, ty2) ) 
					{
						x=(tx<<tilePower)+World.tile_width;
						floatX=x<<tilePower;
						xSpeed=0;
					}
				}
				
				floatY+=ySpeed;
				y=floatY>>tilePower;
						
				tx = (x+w - 2) >> tilePower;
				tx2 = x >> tilePower;		
				if (ySpeed < 0) //check up
				{
					ty=(y)>>tilePower;
						if (myWorld.isSolid(tx, ty) || myWorld.isSolid(tx2, ty)) 
							{
								y=(ty<<tilePower)+World.tile_height;
								floatY=y<<tilePower;
								ySpeed=0;
								
							}
					
				}
				else if (ySpeed >  0){
					
					
					ty=(y+h -2 )>>tilePower;
						if (myWorld.isSolid(tx, ty)|| myWorld.isSolid(tx2, ty) ) 
						{
							y=(ty<<tilePower)-(h-1); 	
							floatY=y<<tilePower;
							ySpeed = 0;
						}
				}
				
			
			
	}	
	
	
	/* ===============
	 * called when player got hit by projectile or monster entity
	 * ===============
	 */
	public void hit() {
		if (healthDelay>0) return;
		
		healthDelay=16;
		health--;
		if (health<=0) health=0;
	}
		
	public int addMemeber(){
		int i=0;
		while (i<team.length && !team[i].deleted) i++;
		if (i<team.length) {
			
			return i;
		}
		return -1;
		
	}



}
